## Prerequisites:
* IDE (Netbeans 8.1) 
* JDK 1.8
* Maven 3.*
* Tomcat 7.*

## External libraries:
* Mockito - Unit test
* Tuckey urlrewritefilter - 4.0.3
* Gson - 2.8.0
* Jackson 1.9.13 - POJO to JSon response
* Spring 4.1.1
* Jersey 2.14
* EhCache 2.9.0
* Logback 1.1.1

## Step to run service
1. Clone the source code from public repository https://bitbucket.org/quanglth/stockpriceservice
2. Go to project directory on your computer, and run the command "mvn clean install"
3. Copy file "<Project Directory>/target/StockPriceService.war" to "<Tomcat Directory>/webapps"
4. Start tomcat by run the shell script name "startup.sh" (for linux) or "startup.bat" (for window) in "<Tomcat Directory>/bin"
5. Test 3 APIs
  
  a. GET http://localhost:8080/StockPriceService/api/v2/GOOG/closePrice?startDate=2017-05-01&endDate=2017-05-24
  
  b. GET http://localhost:8080/StockPriceService/api/v2/FB/200dma?startDate=2016-11-04
  
  c. GET http://localhost:8080/StockPriceService/api/v2/200dmas?startDate=2016-05-22 (With required Header: key = "tickerSymbols", value = "FB, GOOG, GE, BLAH")
