package com.stock.price.rest.endpoint;

import com.stock.price.rest.entity.DayMovingAverage;
import com.stock.price.rest.entity.TickerSymbolClosePrice;
import com.stock.price.rest.errorhandling.InvalidDateException;
import com.stock.price.rest.errorhandling.InvalidTickerSymbolException;
import com.stock.price.rest.service.TickerService;
import com.stock.price.rest.util.DateUtil;
import java.io.IOException;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TickerSymbolEndpointTest {

    @InjectMocks
    private TickerSymbolEndpoint tickerSymbolEndpoint;

    @Mock
    private TickerService tickerService;
    
    @Mock
    private TickerSymbolClosePrice tickerSymbolClosePrice;
    
    @Mock
    private DayMovingAverage dayMovingAverage;

    private final int DELTA_DATE_BEFORE = 200;
    
    @Before
    public void setUp() {
    }

    @Test
    public void testGetClosePrice_Success() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = "2017-05-01";
        String endDate = "2017-05-22";
        when(tickerService.getTickerClosePrice(any(String.class), any(String.class), any(String.class))).thenReturn(tickerSymbolClosePrice);
        tickerSymbolEndpoint.getClosePrice(tickerSymbol, startDate, endDate);
        verify(tickerService).getTickerClosePrice(tickerSymbol, startDate, endDate);
    }
    
    @Test(expected = InvalidDateException.class)
    public void testGetClosePrice_FailStartDate() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = "05-01-2017";
        String endDate = "2017-05-22";
        when(tickerService.getTickerClosePrice(any(String.class), any(String.class), any(String.class))).thenReturn(tickerSymbolClosePrice);
        tickerSymbolEndpoint.getClosePrice(tickerSymbol, startDate, endDate);
        verify(tickerService, never()).getTickerClosePrice(tickerSymbol, startDate, endDate);
    }
    
    @Test(expected = InvalidDateException.class)
    public void testGetClosePrice_FailEndDate() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = "2017-05-01";
        String endDate = "2017-22-05";
        when(tickerService.getTickerClosePrice(any(String.class), any(String.class), any(String.class))).thenReturn(tickerSymbolClosePrice);
        tickerSymbolEndpoint.getClosePrice(tickerSymbol, startDate, endDate);
        verify(tickerService, never()).getTickerClosePrice(tickerSymbol, startDate, endDate);
    }
    
    @Test(expected = InvalidDateException.class)
    public void testGetClosePrice_FailStartDateGreaterThanEndDate() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = "2017-05-01";
        String endDate = "2017-04-05";
        when(tickerService.getTickerClosePrice(any(String.class), any(String.class), any(String.class))).thenReturn(tickerSymbolClosePrice);
        tickerSymbolEndpoint.getClosePrice(tickerSymbol, startDate, endDate);
        verify(tickerService, never()).getTickerClosePrice(tickerSymbol, startDate, endDate);
    }
    
    @Test(expected = InvalidTickerSymbolException.class)
    public void testGetClosePrice_FailTickerSymbol() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbol = "FBII";
        String startDate = "2017-05-01";
        String endDate = "2017-05-22";
        when(tickerService.getTickerClosePrice(any(String.class), any(String.class), any(String.class))).thenThrow(InvalidTickerSymbolException.class);
        tickerSymbolEndpoint.getClosePrice(tickerSymbol, startDate, endDate);
        verify(tickerService).getTickerClosePrice(tickerSymbol, startDate, endDate);
    }
    
    @Test
    public void testGet200DayMovingAverage_Success() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = "2017-05-01";
        when(tickerService.get200DayMovingAvg(any(String.class), any(String.class))).thenReturn(dayMovingAverage);
        tickerSymbolEndpoint.get200DayMovingAverage(tickerSymbol, startDate);
        verify(tickerService).get200DayMovingAvg(tickerSymbol, startDate);
    }
    
    @Test(expected = InvalidDateException.class)
    public void testGet200DayMovingAverage_FailNullStartDate() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = null;
        when(tickerService.get200DayMovingAvg(any(String.class), any(String.class))).thenReturn(dayMovingAverage);
        tickerSymbolEndpoint.get200DayMovingAverage(tickerSymbol, startDate);
        verify(tickerService, never()).get200DayMovingAvg(tickerSymbol, startDate);
    }
    
    @Test(expected = InvalidDateException.class)
    public void testGet200DayMovingAverage_FailWrongStartDateFormat() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = "2017-13-01";
        when(tickerService.get200DayMovingAvg(any(String.class), any(String.class))).thenReturn(dayMovingAverage);
        tickerSymbolEndpoint.get200DayMovingAverage(tickerSymbol, startDate);
        verify(tickerService, never()).get200DayMovingAvg(tickerSymbol, startDate);
    }
    
    @Test(expected = InvalidTickerSymbolException.class)
    public void testGet200DayMovingAverage_FailInvalidTickerSymbol() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbol = "FBII";
        String startDate = "2017-05-01";
        when(tickerService.get200DayMovingAvg(any(String.class), any(String.class))).thenThrow(InvalidTickerSymbolException.class);
        tickerSymbolEndpoint.get200DayMovingAverage(tickerSymbol, startDate);
        verify(tickerService, never()).get200DayMovingAvg(tickerSymbol, startDate);
    }
    
    @Test
    public void testGet200DayMovingAverages_Success() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbols = "FB, GOOG";
        String startDate = "2017-05-01";
        when(tickerService.get200DayMovingAvgs(any(String[].class), any(String.class))).thenReturn(dayMovingAverage);
        tickerSymbolEndpoint.get200DayMovingAverages(tickerSymbols, startDate);
        verify(tickerService).get200DayMovingAvgs(new String[]{"FB", "GOOG"}, startDate);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGet200DayMovingAverages_FailNullTickerSymbol() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbols = null;
        String startDate = "2017-05-01";
        when(tickerService.get200DayMovingAvgs(any(String[].class), any(String.class))).thenReturn(dayMovingAverage);
        tickerSymbolEndpoint.get200DayMovingAverages(tickerSymbols, startDate);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGet200DayMovingAverages_FailEmptyTickerSymbol() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbols = "";
        String startDate = "2017-05-01";
        when(tickerService.get200DayMovingAvgs(any(String[].class), any(String.class))).thenReturn(dayMovingAverage);
        tickerSymbolEndpoint.get200DayMovingAverages(tickerSymbols, startDate);
    }
    
    @Test
    public void testGet200DayMovingAverages_SuccessNullStartDate() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbols = "FB, GOOG";
        String startDate = null;
        when(tickerService.get200DayMovingAvgs(any(String[].class), any(String.class))).thenReturn(dayMovingAverage);
        tickerSymbolEndpoint.get200DayMovingAverages(tickerSymbols, startDate);
        verify(tickerService).get200DayMovingAvgs(new String[]{"FB", "GOOG"}, DateUtil.getTheDateBeforeCurrent(DELTA_DATE_BEFORE));
    }
    
    @Test(expected = InvalidDateException.class)
    public void testGet200DayMovingAverages_FailInvalidFormatStartDate() throws IOException, InvalidDateException, InvalidTickerSymbolException {
        String tickerSymbols = "FB, GOOG";
        String startDate = "2017-15-01";
        when(tickerService.get200DayMovingAvgs(any(String[].class), any(String.class))).thenReturn(dayMovingAverage);
        tickerSymbolEndpoint.get200DayMovingAverages(tickerSymbols, startDate);
        verify(tickerService, never()).get200DayMovingAvgs(any(String[].class), any(String.class));
    }
}
