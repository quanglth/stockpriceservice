/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.service;

import com.stock.price.rest.entity.DataSet;
import com.stock.price.rest.entity.DayMovingAverage;
import com.stock.price.rest.entity.QuandlError;
import com.stock.price.rest.entity.ResponseData;
import com.stock.price.rest.entity.ResponseErrorData;
import com.stock.price.rest.entity.TickerSymbolClosePrice;
import com.stock.price.rest.errorhandling.InvalidTickerSymbolException;
import java.io.IOException;
import java.util.Arrays;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author quangle
 */
@RunWith(MockitoJUnitRunner.class)
public class SPSTickerServiceImplTest {

    @InjectMocks
    private SPSTickerServiceImpl spsTickerServiceImpl;

    @Mock
    private RestCallApiService restCallApiService;

    @Before
    public void setUp() {
    }

    @Test
    public void testGetTickerClosePrice_Success() throws IOException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = "2017-05-01";
        String endDate = "2017-05-22";

        DataSet dataSet = new DataSet();
        dataSet.setId(1l);
        dataSet.setColumn_names(Arrays.asList("Date", "Close"));
        Object ob1 = "2017-05-22";
        Object ob2 = 10;
        Object ob3 = "2017-05-21";
        Object ob4 = 12;
        dataSet.setData(Arrays.asList(Arrays.asList(ob1, ob2), Arrays.asList(ob3, ob4)));
        ResponseData responseData = new ResponseData();
        responseData.setDataset_data(dataSet);
        when(restCallApiService.getResponseFromHttpUrl(any(String.class))).thenReturn(responseData);
        TickerSymbolClosePrice tickerSymbolClosePrice = spsTickerServiceImpl.getTickerClosePrice(tickerSymbol, startDate, endDate);
        Assert.assertEquals(1, tickerSymbolClosePrice.getPrices().size());
        Assert.assertEquals(2, tickerSymbolClosePrice.getPrices().get(0).getDateClose().size());
    }

    @Test(expected = InvalidTickerSymbolException.class)
    public void testGetTickerClosePrice_InvalidTickerSymbol() throws IOException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = "2017-05-01";
        String endDate = "2017-05-22";

        ResponseErrorData responseErrorData = new ResponseErrorData();
        QuandlError quandlError = new QuandlError();
        quandlError.setCode(SPSTickerServiceImpl.INVALID_TICKER_SYMBOL_CODE);
        quandlError.setMessage("message");
        responseErrorData.setQuandl_error(quandlError);
        when(restCallApiService.getResponseFromHttpUrl(any(String.class))).thenReturn(responseErrorData);
        spsTickerServiceImpl.getTickerClosePrice(tickerSymbol, startDate, endDate);
    }

    @Test
    public void testGet200DayMovingAvg_Success() throws IOException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = "2017-05-01";

        DataSet dataSet = new DataSet();
        dataSet.setId(1l);
        dataSet.setColumn_names(Arrays.asList("Date", "Close"));
        Object ob1 = "2017-05-01";
        Object ob2 = 10;
        Object ob3 = "2017-05-02";
        Object ob4 = 12;
        dataSet.setData(Arrays.asList(Arrays.asList(ob1, ob2), Arrays.asList(ob3, ob4)));
        ResponseData responseData = new ResponseData();
        responseData.setDataset_data(dataSet);
        when(restCallApiService.getResponseFromHttpUrl(any(String.class))).thenReturn(responseData);
        DayMovingAverage dayMovingAverage = spsTickerServiceImpl.get200DayMovingAvg(tickerSymbol, startDate);
        Assert.assertEquals(tickerSymbol, dayMovingAverage.getDma200().getTicker());
        Assert.assertEquals("11.0", dayMovingAverage.getDma200().getAvg());
    }

    @Test(expected = InvalidTickerSymbolException.class)
    public void testGet200DayMovingAvg_InvalidTickerSymbol() throws IOException, InvalidTickerSymbolException {
        String tickerSymbol = "FB";
        String startDate = "2017-05-01";

        ResponseErrorData responseErrorData = new ResponseErrorData();
        QuandlError quandlError = new QuandlError();
        quandlError.setCode(SPSTickerServiceImpl.INVALID_TICKER_SYMBOL_CODE);
        quandlError.setMessage("message");
        responseErrorData.setQuandl_error(quandlError);
        when(restCallApiService.getResponseFromHttpUrl(any(String.class))).thenReturn(responseErrorData);
        spsTickerServiceImpl.get200DayMovingAvg(tickerSymbol, startDate);
    }

    @Test
    public void testGet200DayMovingAvgs_Success() throws IOException, InvalidTickerSymbolException {
        String[] tickerSymbols = {"FB", "GOOG"};
        String startDate = "2017-05-01";

        DataSet dataSet = new DataSet();
        dataSet.setId(1l);
        dataSet.setColumn_names(Arrays.asList("Date", "Close"));
        Object ob1 = "2017-05-01";
        Object ob2 = 10;
        Object ob3 = "2017-05-02";
        Object ob4 = 12;
        dataSet.setData(Arrays.asList(Arrays.asList(ob1, ob2), Arrays.asList(ob3, ob4)));
        ResponseData responseData = new ResponseData();
        responseData.setDataset_data(dataSet);

        DataSet dataSet2 = new DataSet();
        dataSet2.setId(1l);
        Object ob5 = 20;
        dataSet2.setColumn_names(Arrays.asList("Date", "Close"));
        dataSet2.setData(Arrays.asList(Arrays.asList(ob1, ob2), Arrays.asList(ob3, ob5)));
        ResponseData responseData2 = new ResponseData();
        responseData2.setDataset_data(dataSet2);
        when(restCallApiService.getResponseFromHttpUrl(any(String.class))).thenReturn(responseData).thenReturn(responseData2);
        DayMovingAverage dayMovingAverage = spsTickerServiceImpl.get200DayMovingAvgs(tickerSymbols, startDate);
        Assert.assertEquals(2, dayMovingAverage.getDma200s().size());
    }

    @Test
    public void testGet200DayMovingAvgs_SuccessWithSomeErrors() throws IOException, InvalidTickerSymbolException {
        String[] tickerSymbols = {"FB", "GOOG", "QAN"};
        String startDate = "2017-05-01";

        DataSet dataSet = new DataSet();
        dataSet.setId(1l);
        dataSet.setColumn_names(Arrays.asList("Date", "Close"));
        Object ob1 = "2017-05-01";
        Object ob2 = 10;
        Object ob3 = "2017-05-02";
        Object ob4 = 12;
        dataSet.setData(Arrays.asList(Arrays.asList(ob1, ob2), Arrays.asList(ob3, ob4)));
        ResponseData responseData = new ResponseData();
        responseData.setDataset_data(dataSet);

        DataSet dataSet2 = new DataSet();
        dataSet2.setId(1l);
        Object ob5 = 20;
        dataSet2.setColumn_names(Arrays.asList("Date", "Close"));
        dataSet2.setData(Arrays.asList(Arrays.asList(ob1, ob2), Arrays.asList(ob3, ob5)));
        ResponseData responseData2 = new ResponseData();
        responseData2.setDataset_data(dataSet2);

        ResponseErrorData responseErrorData = new ResponseErrorData();
        QuandlError quandlError = new QuandlError();
        quandlError.setCode(SPSTickerServiceImpl.INVALID_TICKER_SYMBOL_CODE);
        quandlError.setMessage("message");
        responseErrorData.setQuandl_error(quandlError);

        when(restCallApiService.getResponseFromHttpUrl(any(String.class)))
                .thenReturn(responseData)
                .thenReturn(responseData2)
                .thenReturn(responseErrorData);
        DayMovingAverage dayMovingAverage = spsTickerServiceImpl.get200DayMovingAvgs(tickerSymbols, startDate);
        Assert.assertEquals(2, dayMovingAverage.getDma200s().size());
        Assert.assertEquals(1, dayMovingAverage.getErrors().size());
        Assert.assertEquals(true, dayMovingAverage.getErrors().get(0).contains("is invalid. Please check and provide again"));
    }
}
