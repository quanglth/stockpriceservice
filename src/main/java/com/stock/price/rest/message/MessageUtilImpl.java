/*
 * Copyright 2016
 */
package com.stock.price.rest.message;

import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 *
 * @author quangle
 */
public class MessageUtilImpl implements MessageUtil {
    
    private final Logger logger = LoggerFactory.getLogger(MessageUtilImpl.class);
            
    @Autowired
    public MessageSource messageSource;

    @Override
    public String getMessage(String messageKey) {
        return getLocalizedMessages(messageKey, null);
    }

    @Override
    public String getMessage(String messageKey, Object... arguments) {
        return getLocalizedMessages(messageKey, arguments);
    }

    private String getLocalizedMessages(String messageKey, Object [] arguments) {
        Locale currentLocale = LocaleContextHolder.getLocale();
        logger.debug(String.format("resolving message key '{}' for locale '{}'", messageKey, currentLocale.toString()));
        return messageSource.getMessage(messageKey, arguments, messageKey, currentLocale);
    }
}
