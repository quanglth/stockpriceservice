/*
 * Copyright 2016
 */
package com.stock.price.rest.message;

/**
 *
 * @author quangle
 */
public interface MessageUtil {
    public String getMessage(String messageKey);
    public String getMessage(String messageKey, Object ... arguments);
}
