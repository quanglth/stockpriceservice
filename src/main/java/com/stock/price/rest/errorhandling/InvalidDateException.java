package com.stock.price.rest.errorhandling;

public class InvalidDateException extends Exception {

    private static final long serialVersionUID = -271582034543512905L;

    public InvalidDateException(String message) {
        super(message);
    }
    
    public InvalidDateException() {
        super();
    }
    
    public InvalidDateException(String message, Exception e) {
        super(message, e);
    }
}
