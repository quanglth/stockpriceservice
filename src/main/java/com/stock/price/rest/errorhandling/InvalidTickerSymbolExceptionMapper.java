package com.stock.price.rest.errorhandling;

import javax.ws.rs.ext.Provider;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

@Provider
public class InvalidTickerSymbolExceptionMapper extends SPSExceptionHandler<InvalidTickerSymbolException> {

    private static final String MESSAGE_KEY = "invalid.ticker.symbol";

    @Override
    public long getErrorCode() {
        return 1081l;
    }

    @Override
    public HttpStatus getHttpStatus(InvalidTickerSymbolException e) {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getMessage(InvalidTickerSymbolException e) {
        return StringUtils.isEmpty(e.getMessage()) ? messageUtil.getMessage(MESSAGE_KEY) : e.getMessage();
    }

}
