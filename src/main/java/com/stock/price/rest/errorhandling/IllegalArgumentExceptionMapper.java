package com.stock.price.rest.errorhandling;

import javax.ws.rs.ext.Provider;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

@Provider
public class IllegalArgumentExceptionMapper extends SPSExceptionHandler<IllegalArgumentException> {

    private static final String MESSAGE_KEY = "illgal.argument.value";

    @Override
    public long getErrorCode() {
        return 1083l;
    }

    @Override
    public HttpStatus getHttpStatus(IllegalArgumentException e) {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public String getMessage(IllegalArgumentException e) {
        return StringUtils.isEmpty(e.getMessage()) ? messageUtil.getMessage(MESSAGE_KEY) : e.getMessage();
    }

}
