package com.stock.price.rest.errorhandling;

import java.io.IOException;
import javax.ws.rs.ext.Provider;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

@Provider
public class IOExceptionMapper extends SPSExceptionHandler<IOException> {

    private static final String MESSAGE_KEY = "execute.api.error";

    @Override
    public long getErrorCode() {
        return 1082l;
    }

    @Override
    public HttpStatus getHttpStatus(IOException e) {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getMessage(IOException e) {
        return StringUtils.isEmpty(e.getMessage()) ? messageUtil.getMessage(MESSAGE_KEY) : e.getMessage();
    }

}
