package com.stock.price.rest.errorhandling;

public class InvalidTickerSymbolException extends Exception {

    private static final long serialVersionUID = -27158678543512905L;

    public InvalidTickerSymbolException(String message) {
        super(message);
    }
    
    public InvalidTickerSymbolException() {
        super();
    }
    
    public InvalidTickerSymbolException(String message, Exception e) {
        super(message, e);
    }
}
