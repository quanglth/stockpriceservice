package com.stock.price.rest.errorhandling;

import javax.ws.rs.ext.Provider;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

@Provider
public class InvalidDateExceptionMapper extends SPSExceptionHandler<InvalidDateException> {

    private static final String MESSAGE_KEY = "invalid.date.time";

    @Override
    public long getErrorCode() {
        return 1080l;
    }

    @Override
    public HttpStatus getHttpStatus(InvalidDateException e) {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getMessage(InvalidDateException e) {
        return StringUtils.isEmpty(e.getMessage()) ? messageUtil.getMessage(MESSAGE_KEY) : e.getMessage();
    }

}
