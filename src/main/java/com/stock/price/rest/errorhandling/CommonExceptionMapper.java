/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.errorhandling;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.springframework.http.HttpStatus;

/**
 *
 * @author quangle
 */
@Provider
public class CommonExceptionMapper extends SPSExceptionHandler<Throwable> {

    private final long COMMON_ERROR_CODE = 5000;
    private final String MESSAGE_KEY = "internal.server.error";

    @Override
    public long getErrorCode() {
        return COMMON_ERROR_CODE;
    }

    @Override
    public HttpStatus getHttpStatus(Throwable ex) {
        if (ex instanceof WebApplicationException) {
            return HttpStatus.valueOf(((WebApplicationException) ex).getResponse().getStatus());
        } else {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    @Override
    public String getMessage(Throwable e) {
        return messageUtil.getMessage(MESSAGE_KEY);
    }
}
