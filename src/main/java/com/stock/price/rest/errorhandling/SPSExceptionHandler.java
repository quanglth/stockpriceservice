/*
 * Copyright 2016
 */
package com.stock.price.rest.errorhandling;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import org.apache.commons.lang3.exception.ExceptionUtils;
import com.stock.price.rest.message.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

/**
 *
 * @author quangle
 */
public abstract class SPSExceptionHandler  <F extends Throwable> implements ExceptionMapper<F> {
    
    private final Logger logger = LoggerFactory.getLogger(SPSExceptionHandler.class);
    
    @Autowired
    protected MessageUtil messageUtil;
    
    public abstract long getErrorCode();

    public abstract HttpStatus getHttpStatus(F e);

    public abstract String getMessage(F e);

    public Integer getRetryAfter() {
        return null;
    }
    
    public void log(Throwable e) {
        logger.error("Handling Exception: ", e);
    }
    
    @Override
    public Response toResponse(F e) {
        log(e);
        ErrorMessage error = new ErrorMessage();

        String errorMessage = getMessage(e);
        if (errorMessage == null) {
            errorMessage = "Error details are not available";
        }

        error.setErrorCode(getErrorCode());
        error.setMessage(errorMessage);
        error.setCause(ExceptionUtils.getRootCauseMessage(e));

        Response.ResponseBuilder responseBuilder = Response.status(getHttpStatus(e).value()).
                entity(error).
                type(MediaType.APPLICATION_JSON);

        Integer retryAfter = getRetryAfter();
        if (retryAfter != null) {
            responseBuilder.header("Retry-After", retryAfter);
        }

        return responseBuilder.build();
    }
}
