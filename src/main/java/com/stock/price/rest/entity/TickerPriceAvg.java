/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;

/**
 *
 * @author quangle
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TickerPriceAvg implements Serializable {
    private String ticker;
    private String avg;
    private String errorMessage;
    
    public TickerPriceAvg() {
    }

    public TickerPriceAvg(String ticker, String avg) {
        this.ticker = ticker;
        this.avg = avg;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getAvg() {
        return avg;
    }

    public void setAvg(String avg) {
        this.avg = avg;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
