/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;

/**
 *
 * @author quangle
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseData extends RestResponse implements Serializable {
    private DataSet dataset_data;
    private String ticker;
    private String errorMessage;
    private boolean isSuccess;

    public ResponseData() {
    }

    public ResponseData(DataSet dataset_data) {
        this.dataset_data = dataset_data;
    }

    public DataSet getDataset_data() {
        return dataset_data;
    }

    public void setDataset_data(DataSet dataset_data) {
        this.dataset_data = dataset_data;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
}
