/*
 * Copyright (C) 2017 ItsOn, Inc.
 */
package com.stock.price.rest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author quangle
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TickerSymbolClosePrice implements Serializable {
    private List<DateClose> prices;

    public List<DateClose> getPrices() {
        return prices;
    }

    public void setPrices(List<DateClose> prices) {
        this.prices = prices;
    }
}
