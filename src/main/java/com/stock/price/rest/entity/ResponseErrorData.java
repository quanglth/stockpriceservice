/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;

/**
 *
 * @author quangle
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseErrorData extends RestResponse implements Serializable {
    private QuandlError quandl_error;

    public QuandlError getQuandl_error() {
        return quandl_error;
    }

    public void setQuandl_error(QuandlError quandl_error) {
        this.quandl_error = quandl_error;
    }
}
