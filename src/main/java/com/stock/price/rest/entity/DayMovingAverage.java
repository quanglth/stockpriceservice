/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author quangle
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(Include.NON_NULL)
public class DayMovingAverage implements Serializable {
    
    @JsonProperty("200dma")
    private TickerPriceAvg dma200;
    
    @JsonProperty("200dmas")
    private List<TickerPriceAvg> dma200s;

    private List<String> errors;
    
    public DayMovingAverage() {
    }

    public TickerPriceAvg getDma200() {
        return dma200;
    }

    public void setDma200(TickerPriceAvg dma200) {
        this.dma200 = dma200;
    }

    public List<TickerPriceAvg> getDma200s() {
        return dma200s;
    }

    public void setDma200s(List<TickerPriceAvg> dma200s) {
        this.dma200s = dma200s;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
    
    public void addTickerPriceAvg(TickerPriceAvg tickerPriceAvg) {
        if(dma200s == null) {
            dma200s = new ArrayList<TickerPriceAvg>();
        }
        dma200s.add(tickerPriceAvg);
    }
    
    public void addError(String error) {
        if(errors == null) {
            errors = new ArrayList<String>();
        }
        errors.add(error);
    }
}
