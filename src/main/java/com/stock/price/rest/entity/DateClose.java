/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author quangle
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DateClose implements Serializable {
    private String ticker;
    private List<String[]> dateClose;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public List<String[]> getDateClose() {
        return dateClose;
    }

    public void setDateClose(List<String[]> dateClose) {
        this.dateClose = dateClose;
    }
}
