package com.stock.price.rest;

import org.glassfish.jersey.message.GZipEncoder;
import org.glassfish.jersey.message.filtering.EntityFilteringFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.EncodingFilter;

/**
 * Registers the components to be used by the JAX-RS application
 *
 * @author quangle
 *
 */
public class StockPriceServiceApplication extends ResourceConfig {

    /**
     * Register JAX-RS application components.
     */
    public StockPriceServiceApplication() {

        packages("com.stock.price.rest");
        register(EntityFilteringFeature.class);
        EncodingFilter.enableFor(this, GZipEncoder.class);
    }
}
