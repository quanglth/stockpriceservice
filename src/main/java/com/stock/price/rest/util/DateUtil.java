/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author quangle
 */
public class DateUtil {

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    public static String getCurrent() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        Date date = new Date();
        return sdf.format(date);
    }
    
    public static String getTheDateBeforeCurrent(int deltaDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -deltaDate);
        Date theDateBefore = calendar.getTime();
        return sdf.format(theDateBefore);
    }

    public static String getTheDateAfterADate(String startDate, int deltaDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            Date date = sdf.parse(startDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, deltaDate);
            Date theDateBefore = calendar.getTime();
            return sdf.format(theDateBefore);
        } catch (ParseException ex) {
            return null;
        }
    }
}
