/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.validation;

/**
 *
 * @author quangle
 */
public class IOValidator {
    public static void validate(String valueToValidate, String message) {
        if(valueToValidate == null || valueToValidate.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }
}
