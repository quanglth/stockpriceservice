/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.validation;

import com.stock.price.rest.errorhandling.InvalidDateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author quangle
 */
public class DateValidator {

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    public static void isNull(String dateToValidate, String message) throws InvalidDateException {
        if (dateToValidate == null || dateToValidate.isEmpty()) {
            throw new InvalidDateException(message);
        }
    }

    public static void isValidDate(String dateToValidate, String message) throws InvalidDateException {
        if (dateToValidate == null || dateToValidate.isEmpty()) {
            return;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        sdf.setLenient(false);

        try {
            //if not valid, it will throw ParseException
            Date date = sdf.parse(dateToValidate);
        } catch (ParseException e) {
            throw new InvalidDateException(message);
        }
    }

    public static void compareDates(String startDateStr, String endDateStr, String message) throws InvalidDateException {
        if (startDateStr == null || startDateStr.isEmpty() || endDateStr == null || endDateStr.isEmpty()) {
            return;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            //if not valid, it will throw ParseException
            Date startDate = sdf.parse(startDateStr);
            Date endDate = sdf.parse(endDateStr);
            if (endDate.before(startDate)) {
                throw new InvalidDateException(message);
            }
        } catch (ParseException e) {
            throw new InvalidDateException("Invalid date input");
        }
    }
}
