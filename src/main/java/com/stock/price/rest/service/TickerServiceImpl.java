/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.service;

import com.google.gson.Gson;
import com.stock.price.rest.entity.DateClose;
import com.stock.price.rest.entity.DayMovingAverage;
import com.stock.price.rest.entity.ResponseData;
import com.stock.price.rest.entity.ResponseErrorData;
import com.stock.price.rest.entity.TickerPriceAvg;
import com.stock.price.rest.entity.TickerSymbolClosePrice;
import com.stock.price.rest.errorhandling.InvalidTickerSymbolException;
import com.stock.price.rest.util.DateUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author quangle
 */
public class TickerServiceImpl implements TickerService {

    private final int NTHREADS = 1000;
    private final String INVALID_TICKER_SYMBOL_CODE = "QECx02";
    private final String INVALID_TICKER_SYMBOL_MESSAGE = "The ticker %s is invalid. Please check and provide again";

    @Autowired
    private RestCallApiService restCallApiService;

    @Cacheable(value = "stockPriceLoadCache", key = "#root.targetClass.getCanonicalName()+':'+#root.methodName+':'+#tickerSymbol+':'+#startDate+':'+#endDate")
    public TickerSymbolClosePrice getTickerClosePrice(String tickerSymbol, String startDate, String endDate)
            throws IOException, InvalidTickerSymbolException {
        TickerSymbolClosePrice tickerSymbolClosePrice = new TickerSymbolClosePrice();
        StringBuilder builder = new StringBuilder();
        builder.append("datasets/WIKI/%s/data.json?api_key=DxLhuSpmyFtALz1J98C-&column_index=4");
        if (startDate != null) {
            builder.append("&start_date=");
            builder.append(startDate);
        }
        if (endDate != null) {
            builder.append("&end_date=");
            builder.append(endDate);
        }
        String currentPath = String.format(builder.toString(), tickerSymbol);
        Response response = restCallApiService.execute(currentPath);
        if (response.isSuccessful() && response.body() != null) {
            Gson gson = new Gson();
            ResponseData responeData = gson.fromJson(response.body().string(), ResponseData.class);
            if (responeData != null) {
                List<String[]> dateCloses = new ArrayList<String[]>();
                DateClose dateClose = new DateClose();
                dateClose.setTicker(tickerSymbol);
                for (List<Object> dataItem : responeData.getDataset_data().getData()) {
                    String[] pricePerDate = new String[]{dataItem.get(0).toString(), dataItem.get(1).toString()};
                    dateCloses.add(pricePerDate);
                }
                dateClose.setDateClose(dateCloses);
                tickerSymbolClosePrice.setPrices(Collections.singletonList(dateClose));
            }
        } else {
            throwInvalidTickerSymbolException(response);
        }

        return tickerSymbolClosePrice;
    }

    @Override
    public DayMovingAverage get200DayMovingAvg(String tickerSymbol, String startDate)
            throws IOException, InvalidTickerSymbolException {
        String endDate = DateUtil.getTheDateAfterADate(startDate, 200);
        TickerPriceAvg tickerPriceAvg = getTickerPriceAvg200DayMovingAvg(tickerSymbol, startDate, endDate);
        DayMovingAverage dayMovingAverage = new DayMovingAverage();
        dayMovingAverage.setDma200(tickerPriceAvg);
        return dayMovingAverage;
    }

    @Override
    public DayMovingAverage get200DayMovingAvgs(String[] tickerSymbols, String startDate)
            throws IOException {
        String endDate = DateUtil.getTheDateAfterADate(startDate, 200);
        List<TickerPriceAvg> tickerPriceAvgs = executeAsync(tickerSymbols, startDate, endDate);
        DayMovingAverage dayMovingAverage = new DayMovingAverage();
        for (TickerPriceAvg tickerPriceAvg : tickerPriceAvgs) {
            if (tickerPriceAvg.getErrorMessage() == null) {
                dayMovingAverage.addTickerPriceAvg(tickerPriceAvg);
            } else {
                dayMovingAverage.addError(tickerPriceAvg.getErrorMessage());
            }
        }
        return dayMovingAverage;
    }

    private TickerPriceAvg getTickerPriceAvg200DayMovingAvg(String tickerSymbol, String startDate, String endDate)
            throws IOException, InvalidTickerSymbolException {
        String pathQueryFormat = "datasets/WIKI/%s/data.json?start_date=%s&end_date=%s&column_index=4&api_key=DxLhuSpmyFtALz1J98C-";
        String currentPath = String.format(pathQueryFormat, tickerSymbol, startDate, endDate);
        Response response = restCallApiService.execute(currentPath);
        if (response.isSuccessful() && response.body() != null) {
            Gson gson = new Gson();
            ResponseData responeData = gson.fromJson(response.body().string(), ResponseData.class);
            if (responeData != null) {
                List<List<Object>> dataValues = responeData.getDataset_data().getData();
                float sum = 0;
                for (List<Object> dataItem : dataValues) {
                    sum += Float.parseFloat(dataItem.get(1).toString());
                }
                float average = (dataValues.isEmpty()) ? 0 : sum / dataValues.size();
                return new TickerPriceAvg(tickerSymbol, String.valueOf(average));
            }
        } else {
            throwInvalidTickerSymbolException(response);
        }
        return null;
    }

    private List<TickerPriceAvg> executeAsync(String[] args, final String startDate, final String endDate) {
        ExecutorService executor = Executors.newFixedThreadPool(NTHREADS);
        List<Future<ResponseData>> futures = new ArrayList<Future<ResponseData>>();
        final Gson gson = new Gson();
        for (final String tickerSymbol : args) {
            Future<ResponseData> submit = executor.submit(new Callable<ResponseData>() {
                public ResponseData call() throws Exception {
                    String pathQueryFormat = "datasets/WIKI/%s/data.json?start_date=%s&end_date=%s&column_index=4&api_key=DxLhuSpmyFtALz1J98C-";
                    String currentPath = String.format(pathQueryFormat, tickerSymbol, startDate, endDate);
                    Response response = restCallApiService.execute(currentPath);
                    if (response.isSuccessful() && response.body() != null) {
                        ResponseData responeData = gson.fromJson(response.body().string(), ResponseData.class);

                        /**
                         * There is no data for given startDate, service should
                         * get the data from 200 days ago
                         */
                        if (responeData.getDataset_data().getData().isEmpty()) {
                            String otherStartDate = DateUtil.getTheDateBeforeCurrent(200);
                            String otherEndDate = DateUtil.getCurrent();
                            currentPath = String.format(pathQueryFormat, tickerSymbol, otherStartDate, otherEndDate);

                            // Re-assign respone and response data
                            response = restCallApiService.execute(currentPath);
                            responeData = gson.fromJson(response.body().string(), ResponseData.class);
                        }
                        responeData.setTicker(tickerSymbol);
                        responeData.setIsSuccess(true);
                        return responeData;
                    }
                    if (response.body() != null) {
                        ResponseErrorData responeErrorData = gson.fromJson(response.body().string(), ResponseErrorData.class);
                        if (INVALID_TICKER_SYMBOL_CODE.equals(responeErrorData.getQuandl_error().getCode())) {
                            ResponseData responeData = new ResponseData();
                            responeData.setErrorMessage(String.format(INVALID_TICKER_SYMBOL_MESSAGE, tickerSymbol));
                            responeData.setIsSuccess(false);
                            return responeData;
                        }
                    }
                    return null;
                }
            });
            futures.add(submit);
        }

        return getTickerPriceAvgsFromFutures(futures);
    }
    
    private List<TickerPriceAvg> getTickerPriceAvgsFromFutures(List<Future<ResponseData>> futures) {
        List<TickerPriceAvg> tickerPriceAvgs = new ArrayList<TickerPriceAvg>();
        for (Future<ResponseData> future : futures) {
            TickerPriceAvg tickerPriceAvg = new TickerPriceAvg();
            try {
                ResponseData data = future.get();
                if (data != null) {
                    if (data.isIsSuccess()) {
                        List<List<Object>> dataValues = data.getDataset_data().getData();
                        float sum = 0;
                        for (List<Object> dataItem : dataValues) {
                            sum += Float.parseFloat(dataItem.get(1).toString());
                        }
                        float average = (dataValues.isEmpty()) ? 0 : sum / dataValues.size();
                        tickerPriceAvg.setTicker(data.getTicker());
                        tickerPriceAvg.setAvg(String.valueOf(average));
                    } else {
                        tickerPriceAvg.setErrorMessage(data.getErrorMessage());
                    }
                } else {
                    tickerPriceAvg.setErrorMessage("Empty response on contacting to other microservice. Please contact us");
                }
            } catch (InterruptedException ex) {
                tickerPriceAvg.setErrorMessage("The request is interrupted. Please try again");
            } catch (ExecutionException ex) {
                tickerPriceAvg.setErrorMessage("There is an error on request. Please try again");
            }
            tickerPriceAvgs.add(tickerPriceAvg);
        }
        return tickerPriceAvgs;
    }

    private void throwInvalidTickerSymbolException(Response response) throws InvalidTickerSymbolException, IOException {
        if (response.body() != null) {
            Gson gson = new Gson();
            ResponseErrorData responeErrorData = gson.fromJson(response.body().string(), ResponseErrorData.class);
            if (INVALID_TICKER_SYMBOL_CODE.equals(responeErrorData.getQuandl_error().getCode())) {
                throw new InvalidTickerSymbolException();
            }
        }
    }
}
