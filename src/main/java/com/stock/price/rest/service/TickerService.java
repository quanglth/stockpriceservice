/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.service;

import com.stock.price.rest.entity.DayMovingAverage;
import com.stock.price.rest.entity.TickerSymbolClosePrice;
import com.stock.price.rest.errorhandling.InvalidTickerSymbolException;
import java.io.IOException;

/**
 *
 * @author quangle
 */
public interface TickerService {
    
    public TickerSymbolClosePrice getTickerClosePrice(String tickerSymbol, String startDate, String endDate)
            throws IOException, InvalidTickerSymbolException;
    
    public DayMovingAverage get200DayMovingAvg(String tickerSymbol, String startDate)
            throws IOException, InvalidTickerSymbolException;
    
    public DayMovingAverage get200DayMovingAvgs(String[] tickerSymbols, String startDate)
            throws IOException;
}
