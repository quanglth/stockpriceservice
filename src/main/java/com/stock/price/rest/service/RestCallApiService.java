/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.service;

import com.stock.price.rest.entity.RestResponse;
import java.io.IOException;
import okhttp3.Response;

/**
 *
 * @author quangle
 */
public interface RestCallApiService {
    public Response execute(String path) throws IOException;
    public RestResponse getResponseFromHttpUrl(String path) throws IOException;
}
