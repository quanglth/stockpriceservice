/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.service;

import com.google.gson.Gson;
import com.stock.price.rest.entity.ResponseData;
import com.stock.price.rest.entity.ResponseErrorData;
import com.stock.price.rest.entity.RestResponse;
import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author quangle
 */
public class RestCallApiServiceImpl implements RestCallApiService {

    @Value("${outer.microservice.baseurl}")
    private String baseUrl;

    public RestCallApiServiceImpl() {
    }

    @Override
    public Response execute(String path) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(buildRequestUrl(path))
                .build();

        Response response = client.newCall(request).execute();
        return response;
    }

    @Override
    public RestResponse getResponseFromHttpUrl(String path) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(buildRequestUrl(path))
                .build();

        Response response = client.newCall(request).execute();
        Gson gson = new Gson();
        if (response.isSuccessful() && response.body() != null) {
            ResponseData responeData = gson.fromJson(response.body().string(), ResponseData.class);
            return responeData;
        }
        if(response.body() != null) {
            ResponseErrorData responeErrorData = gson.fromJson(response.body().string(), ResponseErrorData.class);
            return responeErrorData;
        }
        return null;
    }

    private String buildRequestUrl(String path) {
        return String.format("%s/%s", baseUrl, path);
    }
}
