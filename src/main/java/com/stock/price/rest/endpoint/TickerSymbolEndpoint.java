/*
 * Copyright (C) 2017 QuangLe.
 */
package com.stock.price.rest.endpoint;

import com.stock.price.rest.entity.DayMovingAverage;
import com.stock.price.rest.entity.TickerSymbolClosePrice;
import com.stock.price.rest.errorhandling.InvalidDateException;
import com.stock.price.rest.errorhandling.InvalidTickerSymbolException;
import com.stock.price.rest.service.TickerService;
import com.stock.price.rest.util.DateUtil;
import com.stock.price.rest.validation.DateValidator;
import com.stock.price.rest.validation.IOValidator;
import java.io.IOException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author quangle
 */
@Component
@Path("")
public class TickerSymbolEndpoint {

    private final String INVALID_DATE_FORMAT = "The %s equals %s in invalid format. Please provide yyyy-MM-dd";
    private final String INVALID_STARTDATE_ENDDATE = "The endDate must be greater than startDate";
    private final String INVALID_NULL_STARTDATE = "The startDate must be provide. The first possible startDate should be %s";
    private final String EMPTY_TICKER_SYMBOLS = "The ticker symbols must be provided in header key=ticketSymbols";
    private final int DELTA_DATE_BEFORE = 200;

    @Autowired
    protected TickerService tickerService;

    /**
     * Get close price of given ticker symbol in the range of dates
     *
     * @param tickerSymbol [required] Ticker symbol
     * @param startDate - [optional] The start date, will get all data from
     * begin to end date if this is provided null
     * @param endDate - [optional] The end date, will get all data from start
     * date to now if this is provided null
     * @return {@link TickerSymbolClosePrice}
     * @throws IOException
     * @throws InvalidDateException
     * @throws InvalidTickerSymbolException
     */
    @Path("{tickerSymbol}/closePrice")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public TickerSymbolClosePrice getClosePrice(
            @PathParam("tickerSymbol") String tickerSymbol,
            @QueryParam("startDate") String startDate,
            @QueryParam("endDate") String endDate) throws IOException, InvalidDateException, InvalidTickerSymbolException {
        // Validate query params
        DateValidator.isValidDate(startDate, String.format(INVALID_DATE_FORMAT, "startDate", startDate));
        DateValidator.isValidDate(endDate, String.format(INVALID_DATE_FORMAT, "endDate", endDate));
        DateValidator.compareDates(startDate, endDate, INVALID_STARTDATE_ENDDATE);
        return tickerService.getTickerClosePrice(tickerSymbol, startDate, endDate);
    }

    /**
     * Get average prices of 200 days of give ticket symbol from given start date
     * 
     * @param tickerSymbol - [required] Ticker symbol
     * @param startDate - [required] The start date to get average price
     * @return {@link DayMovingAverage}
     * @throws IOException
     * @throws InvalidDateException
     * @throws InvalidTickerSymbolException 
     */
    @Path("{tickerSymbol}/200dma")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public DayMovingAverage get200DayMovingAverage(
            @PathParam("tickerSymbol") String tickerSymbol,
            @QueryParam("startDate") String startDate) throws IOException, InvalidDateException, InvalidTickerSymbolException {
        DateValidator.isNull(startDate, String.format(INVALID_NULL_STARTDATE, DateUtil.getTheDateBeforeCurrent(DELTA_DATE_BEFORE)));
        DateValidator.isValidDate(startDate, String.format(INVALID_DATE_FORMAT, "startDate", startDate));
        return tickerService.get200DayMovingAvg(tickerSymbol, startDate);
    }

    /**
     * Get average prices of 200 days of give ticket symbols from given start date
     * 
     * @param tickerSymbols - [required] Ticker symbols in Header, separated by comma (,)
     * @param startDate - [optional] the start date, 200 day before current will be applied if this is null
     * @return {@link DayMovingAverage}
     * @throws IOException
     * @throws InvalidDateException
     * @throws InvalidTickerSymbolException 
     */
    @Path("200dmas")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public DayMovingAverage get200DayMovingAverages(
            @HeaderParam("tickerSymbols") String tickerSymbols,
            @QueryParam("startDate") String startDate) throws IOException, InvalidDateException, InvalidTickerSymbolException {
        IOValidator.validate(tickerSymbols, EMPTY_TICKER_SYMBOLS);
        if (startDate == null) {
            startDate = DateUtil.getTheDateBeforeCurrent(DELTA_DATE_BEFORE);
        } else {
            DateValidator.isValidDate(startDate, String.format(INVALID_DATE_FORMAT, "startDate", startDate));
        }
        String[] ticketSymbolArray = tickerSymbols.replaceAll(" ", "").split(",");
        return tickerService.get200DayMovingAvgs(ticketSymbolArray, startDate);
    }
}
